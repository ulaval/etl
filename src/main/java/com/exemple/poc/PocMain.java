package com.exemple.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocMain {

	public static void main(String[] args) {
		SpringApplication.run(PocMain.class, args);
	}
}
